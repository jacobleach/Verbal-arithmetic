--Solves TWO + TWO = FOUR, where T, F can not be zero with no duplicate values.

t = [1..9]
w = [0..9]
o = [0..9]
f = [1..9]
u = [0..9]
r = [0..9]
 
ans = filter (/= []) [
  if ((sum [t' * 200 ,w' * 20,o' * 2] == sum [f' * 1000 ,o' * 100 ,u' * 10,r'])
    && (t' /= w') 
    && (t' /= o') 
    && (t' /= f') 
    && (t' /= u') 
    && (t' /= r') 
    && (w' /= o') 
    && (w' /= f') 
    && (w'/= u') 
    && (w' /= r') 
    && (o' /= f') 
    && (o' /= u') 
    && (o' /= r') 
    && (f' /= u') 
    && (f' /= r') 
    && (u' /= r')) then [t', w', o', f', o', u', r'] 
  else [] | t' <- t, w' <- w, o' <- o, f' <- f, u' <- u, r' <- r]

